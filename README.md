### **Thoughts**

- Es wird beim anlegen oder ändern geprüft ob dueDate älter als heute ist. Denn das macht keinen Sinn.
- Für das Filtern nach Kategorien haben wir eine dynamische Liste der Kategorien zu Verfügung gestellt.
Sodass der Benutzer direkt mit einem Klick nach den Verfügbaren Kategorien Filtern kann.
- Aus Einfachheitsgründen werden beim löschen eines Todo alle Todos neu geschrieben und die Id's neu gesetzt
so dass es keine Lücken gibt bei den Id's. Die id's würden wir im normallfall natürlich nicht ändern.

### **Geplante Arbeiten**

- add.jsp und edit.jsp in einer einzigen Datei zusammenfügen