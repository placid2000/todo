package User;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JAXBHandler {
    public static void marshal(List<TodoUser> users, File selectedFile) throws IOException, JAXBException {
        JAXBContext context;
        BufferedWriter writer = null;
        writer = new BufferedWriter(new FileWriter(selectedFile));
        context = JAXBContext.newInstance(TodoUsers.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(new TodoUsers(users), writer);
        writer.close();
    }

    public static List<TodoUser> unmarshal(File importFile) throws JAXBException {
        TodoUsers users = new TodoUsers();

        JAXBContext context = JAXBContext.newInstance(TodoUsers.class);
        Unmarshaller um = context.createUnmarshaller();
        users = (TodoUsers) um.unmarshal(importFile);

        return users.getTodoUsers();
    }
}
