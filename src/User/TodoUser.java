package User;

import javafx.application.Application;
import todo.Todo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import Logger.ApplicationLogger;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "user")
public class TodoUser {
    //@XmlElement
    private String name;
    private String password;
    @XmlElement(name = "todo", type = Todo.class)
    private List<Todo> todos;

    public TodoUser() {

    }

    public TodoUser(String name, String password) {
        this.name = name;
        this.password = password;
        this.todos = new LinkedList<>();

        ApplicationLogger.init();
    }

    public List<Todo> getAllTodos() {
        List<Todo> tempTodo = null;
        if (todos != null) {
            tempTodo = todos;
        }

        return tempTodo;
    }

    public List<Todo> getTodos(String category) {
        List<Todo> tempTodo = null;

        if (category.equals("offen")) {
            tempTodo = getopenTodos();
        } else if (category.equals("erledigt")) {
            tempTodo = getclosedTodos();
        } else {
            if (todos != null) {
                tempTodo = todos.stream()
                        .filter(t -> category.equals(t.getCategory()))
                        .filter(t -> !t.isCompleted())
                        .filter(t -> t.getDueDate() != null)
                        .sorted((o1, o2) -> o1.getDueDateLocal().compareTo(o2.getDueDateLocal()))
                        .collect(Collectors.toList());

                List<Todo> tempTodoEmptyDate = todos.stream()
                        .filter(t -> category.equals(t.getCategory()))
                        .filter(t -> !t.isCompleted())
                        .filter(t -> t.getDueDate() == null)
                        .collect(Collectors.toList());

                for (Todo todo: tempTodoEmptyDate) {
                    tempTodo.add(todo);
                }
            }
        }

        return tempTodo;
    }

    private List<Todo> getopenTodos() {
        List<Todo> tempTodo = new LinkedList<>();
        if (todos != null) {
            tempTodo = todos.stream()
                    .filter(t -> !t.isCompleted())
                    .filter(t -> t.getDueDate() != null)
                    .sorted((o1, o2) -> o1.getDueDateLocal().compareTo(o2.getDueDateLocal()))
                    .collect(Collectors.toList());

            List<Todo> tempTodoEmptyDate = todos.stream()
                    .filter(t -> !t.isCompleted())
                    .filter(t -> t.getDueDate() == null)
                    .collect(Collectors.toList());

            for (Todo todo: tempTodoEmptyDate) {
                tempTodo.add(todo);
            }
        }

        return tempTodo;

    }

    private List<Todo> getclosedTodos() {
        List<Todo> tempTodo = null;
        if (todos != null) {
            tempTodo =  todos.stream()
                    .filter(Todo::isCompleted)
                    .filter(t -> t.getDueDate() != null)
                    .sorted((o1, o2) -> o1.getDueDateLocal().compareTo(o2.getDueDateLocal()))
                    .collect(Collectors.toList());

            List<Todo> tempTodoEmptyDate = todos.stream()
                    .filter(Todo::isCompleted)
                    .filter(t -> t.getDueDate() == null)
                    .collect(Collectors.toList());

            for (Todo todo: tempTodoEmptyDate) {
                tempTodo.add(todo);
            }
        }

        return tempTodo;
    }

    public void addTodo(Todo todo) {
        todos.add(todo);
    }

    public void updateTodo(Todo todo) {
        for (Todo updateTodo : todos) {
            if (updateTodo.getId() == todo.getId()) {
                todos.set(updateTodo.getId() -1, todo);
                return;
            }
        }
    }

    public void removeTodo(int id) {
        for(Todo todo : todos) {
            if(todo.getId() == id) {
                todos.remove(todo);
                sortTodoList();
                break;
            }
        }
    }

    private void sortTodoList() {
        int i = 1;
        for (Todo todo: todos) {
            todo.setId(i++);
        }
    }

    public List<String> getCategories() {
        List<String> categoryList = new LinkedList<>();

        if (todos != null) {
            for (Todo todo: todos) {
                categoryList.add(todo.getCategory());
            }

            // The following don't work
            //todos.forEach(t->categoryList.add(t.getCategory()));

            categoryList = categoryList.stream()
                    .filter(Objects::nonNull)
                    .distinct()
                    .collect(Collectors.toList());
        }
        return categoryList;

    }

    public int getNextTodoID() {
        int id = (todos == null) ? 1 : (todos.size() + 1);
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Todo> getTodos() {
        return todos;
    }

    public void setTodos(List<Todo> todos) {
        this.todos = todos;
    }
}