package User;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "users")
public class TodoUsers {
    @XmlElement(name = "user", type = TodoUser.class)
    private List<TodoUser> todoUsers = new LinkedList<>();

    public TodoUsers() { }

    public TodoUsers(List<TodoUser> users) {
        this.todoUsers = users;
    }

    public List<TodoUser> getTodoUsers() {
        return todoUsers;
    }

    public void setTodoUsers(List<TodoUser> todoUsers) {
        this.todoUsers = todoUsers;
    }
}
