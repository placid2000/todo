package JUnit;

import User.TodoUser;
import todo.Todo;

import java.util.LinkedList;
import java.util.List;

public class TestFactory {

    public TodoUser getTestUser() {
        TodoUser user = new TodoUser("Max", "Mustermann");

        // int id, String title, String category, String dueDate, boolean important
        Todo todo_1 = new Todo(1, "Todo Application", "Schule", "22.02.2019", true);
        Todo todo_2 = new Todo(2, "Skizze anfertigen", "Schule", null, false);
        Todo todo_3 = new Todo(3, "Angebot Einholen", "Arbeit", "21.2.2019", true);
        Todo todo_4 = new Todo(4, "Pitch erstellen", "Arbeit", "25.02.2019", true);
        Todo todo_5 = new Todo(5, "Auto abholen", "Privat", "19.02.2019", true);
        Todo todo_6 = new Todo(6, "Auto waschen", "Privat", "01.03.2019", false);
        Todo todo_7 = new Todo(7, "Handy kaufen", "Privat", "17.03.2019", true);
        Todo todo_8 = new Todo(8, "Schlafen", null, null, false);
        Todo todo_9 = new Todo(9, "Einkaufen", "Privat", "21.2.2019", true);
        Todo todo_10 = new Todo(10, "Putzen", "Privat", "18.02.2019", false);
        Todo todo_11 = new Todo(11, "Hausaufgaben machen", "Schule", "25.02.2019", true);
        Todo todo_12 = new Todo(12, "Hausaufgaben abgeben", "Schule", "04.03.2019", true);
        Todo todo_13 = new Todo(13, "Waschen", "Privat", null, true);
        Todo todo_14 = new Todo(14, "TV reparieren", null, "1.04.2019", false);
        Todo todo_15 = new Todo(15, "Keller aufräumen", "Privat", "25.03.2019", true);
        Todo todo_16 = new Todo(16, "Meeting", "Arbeit", "17.02.2019", false);

        user.addTodo(todo_1);
        user.addTodo(todo_2);
        user.addTodo(todo_3);
        user.addTodo(todo_4);
        user.addTodo(todo_5);
        user.addTodo(todo_6);
        user.addTodo(todo_7);
        user.addTodo(todo_8);
        user.addTodo(todo_9);
        user.addTodo(todo_10);
        user.addTodo(todo_11);
        user.addTodo(todo_12);
        user.addTodo(todo_13);
        user.addTodo(todo_14);
        user.addTodo(todo_15);
        user.addTodo(todo_16);

        return user;
    }
}
