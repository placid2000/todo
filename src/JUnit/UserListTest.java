package JUnit;

import Service.ValidatorService;
import User.TodoUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import todo.Todo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UserListTest {
    @Test
    public void addUserTest() {
        /*
        Testen, das mehrere User in die List hinzugefügt werden können
         */
        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        List<TodoUser> users = new LinkedList<>();
        users.add(user);
        assertThat(users.size(), is(1));

        // add second TodoUser to list
        TodoUser newTodoUser = new TodoUser("Bob", "Harris");
        users.add(newTodoUser);
        assertThat(users.size(), is(2));
    }

    @Test
    public void getCategoriesTest() {
        /*
        Testen, dass eine Kategorie nur einmal zurückgegben wird und das es keine leere Kategorie gitb
         */
        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        List<String> expected = Arrays.asList("Schule", "Arbeit", "Privat");

        List<String> actual = user.getCategories();

        assertThat(actual, is(expected));
    }

    @Test
    public void addTodoTest() {
        /*
        Testen, das dem User mehrere Todos hinzugefügt werden können und die ID's unique sind
         */
        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        assertThat(user.getTodos().size(), is(16));
    }

    @Test
    public void addFalseTitleTodoTest() {
        /*
        Testen, wenn kein Titel gesetzt ist, darf das Todo nicht in die Liste hinzugefügt werden
         */
        TodoUser user = new TodoUser("Max", "Mustermann");
        Todo todo = new Todo(1, "", "Arbeit", "21.04.2019", true);

        ValidatorService validator = new ValidatorService();
        Map<String, String> errors = validator.validateRequestParam(todo.getTitle(), todo.getDueDate());

        if(errors.size() == 0) {
            user.addTodo(todo);
        }

        assertThat(user.getTodos().size(), is(0));
    }

    @Test
    public void addFalseDueDateTodoTest() {
        /*
        Testen, wenn das Due Date früher ist, als heute, darf das Todo nicht hinzugefügt werden.
         */
        TodoUser user = new TodoUser("Max", "Mustermann");
        Todo todo = new Todo(1, "Todo Appikation", "Arbeit", "01.01.2018", true);

        ValidatorService validator = new ValidatorService();
        Map<String, String> errors = validator.validateRequestParam(todo.getTitle(), todo.getDueDate());

        if(errors.size() == 0) {
            user.addTodo(todo);
        }

        assertThat(user.getTodos().size(), is(0));

    }

    @Test
    public void removeTodoTest() {
        /*
        Testen, dass Todos gelöscht werden können
         */
        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        int tempListSize = user.getTodos().size();

        user.removeTodo(9);

        assertThat(user.getTodos().size(), is(tempListSize - 1));
    }

    @Test
    public void removeTodoSortTest() {
        /*
        Testen, dass Todos gelöscht werden können und die ID's in der Liste sortiert werden
         */
        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        int expected = 1;

        user.removeTodo(9);

        for (Todo todo: user.getTodos()) {
            assertThat(todo.getId(), is(expected));
            expected++;
        }
    }

    @Test
    public void editTodoTest() {
        /*
        Testen, dass Todos bearbeitet werden können und
         */

    }

    @Test
    public void getOpenTodosTest() {
        /*
        Testen, dass nur die offenen Todos zurückgegeben werden in der Reihenfolge nach folgenden Kriterien:
            - Zuerst die Todos mit einem Fälligkeitsdatum (Due Date) geordnet
            - Dann ohne Fälligkeitsdatum nach ID
         */
        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        List<Todo> expected = user.getTodos().stream()
                .filter(t -> t.getId() % 2 == 0)
                .collect(Collectors.toList());


        // Jedes 2. Todo wird auf complete gesetzt
        for (Todo todo : user.getTodos()) {
            if (todo.getId() % 2 == 0) {
                todo.setCompleted(true);
            }
        }

        List<Todo> actual = user.getTodos("offen");

        assertThat(actual.size(), is(expected.size()));
    }

    @Test
    public void getClosedTodos() {
        /*
        Testen, dass nur die geschlossenen Todos zurückgegeben werden in der Reihenfolge nach folgenden Kriterien:
            - Zuerst die Todos mit einem Fälligkeitsdatum (Due Date) geordnet
            - Dann ohne Fälligkeitsdatum nach ID
         */

        TestFactory testdata = new TestFactory();
        TodoUser user = testdata.getTestUser();

        List<Todo> expected = user.getTodos().stream()
                .filter(t -> t.getId() % 3 == 0)
                .collect(Collectors.toList());

        for (Todo todo : user.getTodos()) {
            if (todo.getId() % 3 == 0) {
                todo.setCompleted(true);
            }
        }

        List<Todo> actual = user.getTodos("erledigt");

        assertThat(actual.size(), is(expected.size()));
    }

    @Test
    public void getCategoryTodos() {
        /*
        Testen, dass nur die Todos einer spezifischen Kategorie zurückgegeben werden in der Reihenfolge nach folgenden Kriterien:
            - Zuerst die Todos mit einem Fälligkeitsdatum (Due Date) geordnet
            - Dann ohne Fälligkeitsdatum nach ID
         */

    }
}
