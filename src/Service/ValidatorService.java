package Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class ValidatorService {

    public Map<String, String> validateRequestParam(String title, String dueDate) {
        HashMap<String, String> errors = new HashMap<>();

        if(title.equals("")) {
            errors.put("title", "Titel darf nicht leer sein");
        }

        if(dueDate != null) {
            String[] dueDateSplit = dueDate.split("\\.");
            LocalDate tempDueDate= LocalDate.of(Integer.parseInt(dueDateSplit[2]), Integer.parseInt(dueDateSplit[1]), Integer.parseInt(dueDateSplit[0]));
            LocalDate now = LocalDate.now();

            if (now.isAfter(tempDueDate)) {
                errors.put("dueDate", "Frist darf nicht älter als heute sein");
            }
        }

        return errors;
    }
}
