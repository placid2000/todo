package todo;

import User.JAXBHandler;
import User.TodoUser;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/complete")
public class CompleteServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        TodoUser user = (TodoUser) request.getSession().getAttribute("user");

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("Complete ID: " + id);
        user.getTodos().get(id - 1).setCompleted(true);

        // store new data to XML
        try {
            JAXBHandler.marshal((List<TodoUser>) getServletContext().getAttribute("users"), new File("users.xml"));
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + "/todo");
    }
}
