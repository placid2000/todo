package todo;

import User.TodoUser;
import User.JAXBHandler;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Logger.ApplicationLogger;
import javafx.application.Application;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private final static Logger logger;
    static {
        logger = Logger.getLogger(ApplicationLogger.class.getName());
    }

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("Login Servlet Init");
        ApplicationLogger.init();
        List<TodoUser> users = new LinkedList<>();

        File xmlFile = new File("users.xml");
        if (xmlFile.exists()) {
            try {
                users = JAXBHandler.unmarshal(xmlFile);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }

        ServletContext context = getServletContext();
        context.setAttribute("users", users);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.sendRedirect(request.getContextPath() + "/login.jsp");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        List<TodoUser> tempUserList = (List<TodoUser>) getServletContext().getAttribute("users");
        boolean isRegistered = false;

        if (name == null || name.isEmpty() || password == null || password.isEmpty()) {
            request.getRequestDispatcher("/login.jsp").forward(request, response);
            logger.log(Level.SEVERE, "Username oder Password dürfen nicht null sein");

        } else {
            if (tempUserList.isEmpty()) {
                TodoUser tempUser = new TodoUser(name, password);
                tempUserList.add(tempUser);
                logger.log(Level.INFO, "Neuer User angemeldet mit Name: " + name + " und Passwort: " + password);

                //getServletContext().setAttribute("users", tempUserList);
                request.getSession().setAttribute("user", tempUser);

                try {
                    JAXBHandler.marshal(tempUserList, new File("users.xml"));
                } catch (JAXBException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                for (TodoUser todouser: tempUserList) {
                    if (todouser.getName().equals(name) && todouser.getPassword().equals(password)) {
                        isRegistered = true;
                        logger.log(Level.INFO, "User '" + name + "' wieder eingeloggt");
                        request.getSession().setAttribute("user", todouser);
                    }
                }

                if (!isRegistered) {
                    TodoUser tempUser = new TodoUser(name, password);
                    tempUserList.add(tempUser);
                    logger.log(Level.INFO, "Neuer User angemeldet mit Name: " + name + " und Passwort: " + password);
                    //getServletContext().setAttribute("users", tempUserList);

                    request.getSession().setAttribute("user", tempUser);

                    try {
                        JAXBHandler.marshal(tempUserList, new File("users.xml"));
                        logger.log(Level.INFO, "Write users to XML");
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        response.sendRedirect(request.getContextPath() + "/todo");
    }
}