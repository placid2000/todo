package todo;

import User.TodoUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet(urlPatterns = "/todo")
public class TodoServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        TodoUser user = (TodoUser) request.getSession().getAttribute("user");
        List<String> categories;
        List<Todo> todoList = new LinkedList<>();
        String filter = (request.getParameter("filter") == null) ? "offen": request.getParameter("filter");
        System.out.println(filter);

        if (filter != null) {
            todoList = user.getTodos(filter);
        } else {
            todoList = user.getAllTodos();
        }

        categories = user.getCategories();
        
        request.setAttribute("todoList", todoList);
        request.setAttribute("categories", categories);

        request.setAttribute("template", "list");
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}