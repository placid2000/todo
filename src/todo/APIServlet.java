package todo;

import User.JAXBHandler;
import User.TodoUser;
import com.google.gson.*;

import java.io.BufferedReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import static java.nio.charset.StandardCharsets.ISO_8859_1;


@WebServlet("/api/*")
public class APIServlet extends HttpServlet{
    private List<TodoUser> users;
    private TodoUser todoUser;
    private Todo todo;
    private String responseToClient;
    private String category;


    @Override
    public void init() throws ServletException {
        users = new LinkedList<>();
        if(getServletContext().getAttribute("users")!=null){
            users = (List<TodoUser>)getServletContext().getAttribute("users");
        }else{
            File xmlFile = new File("users.xml");
            if (xmlFile.exists()) {
                try {
                    users = JAXBHandler.unmarshal(xmlFile);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response){
        String pathInfo = request.getPathInfo();
        String[] pathSplitted = pathInfo.split("/");

        if(pathInfo.equals("/todos")&&pathSplitted.length==2){
            if(request.getHeader("Accept").equals("application/json")) {
                String[] userdata = getUserData(request);
                todoUser = loadUser(userdata[0], userdata[1]);
                if(todoUser==null){
                    responseToClient = "user not authorized";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }else {
                    Gson gson = new Gson();
                    List<Todo> linkedTodos;
                    List stringTodo = new ArrayList<>();
                    if (todoUser.getTodos() != null) {
                        category = request.getParameter("category");
                        if (category != null) {
                            linkedTodos = todoUser.getTodos(category);
                        } else {
                            linkedTodos = todoUser.getAllTodos();
                        }
                        for (Todo todo : linkedTodos) {
                            stringTodo.add(gson.toJsonTree(todo, Todo.class).toString());
                        }
                        responseToClient = stringTodo.toString();
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        responseToClient = "[]";
                        response.setContentType("application/json");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_OK);
                    }
                }
            }else{
                responseToClient = "unsupported accept type";
                response.setContentType("text/plain");
                response.setHeader("Content-Transfer-Encoding", "UTF-8");
                response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            }
        }
        int todoID = 0;
        if(pathSplitted[1].equals("todos")&&pathSplitted.length==3){
            if(request.getHeader("Accept").equals("application/json")) {
                String[] userdata = getUserData(request);
                todoUser = loadUser(userdata[0], userdata[1]);
                if(todoUser==null){
                    responseToClient = "user not authorized";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }else {
                    try {
                        todoID = Integer.parseInt(pathSplitted[2]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(todoID > 0 && todoID <= todoUser.getTodos().size()){
                        for (Todo singleTodo : todoUser.getTodos()) {
                            if (singleTodo.getId() == todoID) {
                                todo = singleTodo;
                                break;
                            }
                        }
                        if (todo != null) {
                            Gson gson = new Gson();
                            responseToClient = gson.toJson(todo, Todo.class);
                            response.setContentType("application/json");
                            response.setHeader("Content-Transfer-Encoding", "UTF-8");
                            response.setStatus(HttpServletResponse.SC_OK);
                        }
                    }else{
                        responseToClient = "todo not found";
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }
            }else{
                responseToClient = "unsupported accept type";
                response.setContentType("text/plain");
                response.setHeader("Content-Transfer-Encoding", "UTF-8");
                response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            }
        }
        if(pathInfo.equals("/categories")){
            if(request.getHeader("Accept").equals("application/json")) {
                String[] userdata = getUserData(request);
                todoUser = loadUser(userdata[0],userdata[1]);
                if(todoUser==null){
                    responseToClient = "user not authorized";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }else {
                    List linkedTodos;
                    if (todoUser.getCategories() != null) {
                        linkedTodos = todoUser.getCategories();
                        responseToClient = linkedTodos.toString();
                    } else {
                        responseToClient = "Error. No categories available";
                    }

                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_OK);
                }
            }else{
                responseToClient = "unsupported accept type";
                response.setContentType("text/plain");
                response.setHeader("Content-Transfer-Encoding", "UTF-8");
                response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            }

        }
        try {
            answer_Http(response, responseToClient);
        }catch(IOException e){
            System.out.println("Couldn't send an answer. There is a connection error!");
        }
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        if(request.getPathInfo().equals("/users")) {
            if(request.getContentType().equals("application/json")) {


                StringBuffer jb = new StringBuffer();
                String line;
                try {
                    BufferedReader reader = request.getReader();
                    while ((line = reader.readLine()) != null)
                        jb.append(line);
                } catch (Exception e) {
                    System.out.println("Daten konnten nicht gelesen werden");
                    System.out.print(e.toString());
                }

                try {
                    Gson gson = new Gson();
                    todoUser = gson.fromJson(jb.toString(), TodoUser.class);
                    if(todoUser.getName()==null||todoUser.getPassword()==null){
                        throw new Exception("Missing Name or Password");
                    }
                    //Prüfe ob beide Eingaben korrekt sind sonst einen Fehler schmeissen

                } catch (Exception e) {
                    responseToClient = "invalid user data";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
                if(responseToClient==null){
                    for(TodoUser singleTodoUser : users)
                    {
                        if (singleTodoUser.getName().equals(todoUser.getName())) {
                            responseToClient = "a user with the same name already exists";
                            response.setContentType("text/plain");
                            response.setHeader("Content-Transfer-Encoding", "UTF-8");
                            response.setStatus(HttpServletResponse.SC_CONFLICT);
                            break;
                        }
                    }
                    if (responseToClient==null) {
                        users.add(new TodoUser(todoUser.getName(), todoUser.getPassword()));
                        getServletContext().setAttribute("users",users);
                        responseToClient = "User registered";
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_CREATED);
                    }
                }



            }else{
                responseToClient = "unsupported content type";
                response.setContentType("text/plain");
                response.setHeader("Content-Transfer-Encoding", "UTF-8");
                response.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            }

        }
        if(request.getPathInfo().equals("/todos")) {
            if(request.getContentType().equals("application/json")) {
                String[] userdata = getUserData(request);
                todoUser = loadUser(userdata[0], userdata[1]);
                if(todoUser==null){
                    responseToClient = "user not authorized";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }else {
                    StringBuffer jb = new StringBuffer();
                    String line;
                    try {
                        BufferedReader reader = request.getReader();
                        while ((line = reader.readLine()) != null)
                            jb.append(line);
                        //System.out.println(jb.toString());
                    } catch (Exception e) {
                        responseToClient = "invalid todo data";
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    try {
                        Gson gson = new Gson();
                        todo = gson.fromJson(jb.toString(), Todo.class);
                        todo.setId(todoUser.getTodos().size() + 1);

                    } catch (Exception e) {
                        responseToClient = "invalid todo data";
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                    if (responseToClient == null) {
                        todoUser.addTodo(todo);
                        responseToClient = "Todo gespeichert";

                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_OK);
                    }
                }

            }else{
                responseToClient = "unsupported content type";
                response.setContentType("text/plain");
                response.setHeader("Content-Transfer-Encoding", "UTF-8");
                response.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            }
            try {
                answer_Http(response, responseToClient);
            }catch(IOException e){
                System.out.println("Couldn't send an answer. There is a connection error!");
            }

        }
        try {
            answer_Http(response, responseToClient);
        }catch(IOException e){
            System.out.println("Couldn't send an answer. There is a connection error!");
        }
        updateXML();
        responseToClient = null;

    }
    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response){
        String pathInfo = request.getPathInfo();
        String[] pathSplitted = pathInfo.split("/");
        if(pathSplitted[1].equals("todos")&&pathSplitted.length==3) {
            String[] userdata = getUserData(request);
            todoUser = loadUser(userdata[0], userdata[1]);
            if(todoUser==null){
                responseToClient = "user not authorized";
                response.setContentType("text/plain");
                response.setHeader("Content-Transfer-Encoding", "UTF-8");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }else {
                //Wie Post. Statt todo anlegen, updaten.
                StringBuffer jb = new StringBuffer();
                String line;
                try {
                    BufferedReader reader = request.getReader();
                    while ((line = reader.readLine()) != null)
                        jb.append(line);

                } catch (Exception e) {
                    responseToClient = "invalid todo data";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }

                try {
                    Gson gson = new Gson();
                    todo = gson.fromJson(jb.toString(), Todo.class);
                    todo.setId(Integer.parseInt(pathSplitted[2]));
                } catch (Exception e) {
                    responseToClient = "invalid todo data";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
                if(responseToClient == null) {
                    if(todoUser.getTodos().size()>0&& todo.getId()<=todoUser.getTodos().size()&&todo.getId()>0) {
                        todoUser.updateTodo(todo);
                        responseToClient = "todo updated";
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    }else{
                        responseToClient="todo not found";
                        response.setContentType("text/plain");
                        response.setHeader("Content-Transfer-Encoding", "UTF-8");
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }
            }
        }else{
            responseToClient="todo not found";
            response.setContentType("text/plain");
            response.setHeader("Content-Transfer-Encoding", "UTF-8");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        try {
            answer_Http(response, responseToClient);

        }catch(IOException e){
            System.out.println("Couldn't send an answer. There is a connection error!");
        }
        updateXML();
    }
    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response){

        String pathInfo = request.getPathInfo();
        String[] pathSplitted = pathInfo.split("/");

        String[] userdata = getUserData(request);
        todoUser = loadUser(userdata[0],userdata[1]);
        if(todoUser==null){
            responseToClient = "user not authorized";
            response.setContentType("text/plain");
            response.setHeader("Content-Transfer-Encoding", "UTF-8");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }else{

            if (pathSplitted[1].equals("todos") && pathSplitted.length == 3) {

                try {
                    int idTodo = Integer.parseInt(pathSplitted[2]);
                    if (idTodo >= 1 && idTodo <= todoUser.getTodos().size()) {
                        todoUser.removeTodo(Integer.parseInt(pathSplitted[2]));
                    } else {
                        throw new Exception("Invalid ID");
                    }
                    responseToClient = "todo removed";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                } catch (Exception e) {
                    responseToClient = "todo not found";
                    response.setContentType("text/plain");
                    response.setHeader("Content-Transfer-Encoding", "UTF-8");
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        }
        //Answer with Success etc
        try {
            answer_Http(response, responseToClient);

        }catch(IOException e){
            System.out.println("Couldn't send an answer. There is a connection error!");
        }
        updateXML();
    }
    private String[] getUserData(HttpServletRequest request){
        String authHeader = request.getHeader("Authorization");
        String credentials = authHeader.split(" ")[1];
        credentials = new String(DatatypeConverter.parseBase64Binary(credentials), ISO_8859_1);
        String username = credentials.split(":")[0];
        String password = credentials.split(":")[1];
        String[] userdata = new String[2];
        userdata[0] = username;
        userdata[1] = password;
        return userdata;
    }
    private TodoUser loadUser(String name, String password){
        //GIBT FEHLER FALLS USER NICHT VORHANDEN
        TodoUser todoUserloaded = null;
        for(TodoUser tu : users){
            if(tu.getName().equals(name)&&tu.getPassword().equals(password)){
                todoUserloaded = tu;
                break;
            }
        }
        return todoUserloaded;
    }
    private void answer_Http(HttpServletResponse response,String message) throws IOException{
        response.getWriter().write(message);
        response.getWriter().flush();
        response.getWriter().close();
        responseToClient = null;
        todoUser=null;
        todo=null;

    }
    private void updateXML(){
        try {
            JAXBHandler.marshal((List<TodoUser>) getServletContext().getAttribute("users"), new File("users.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
