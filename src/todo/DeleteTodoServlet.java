package todo;


import User.JAXBHandler;
import User.TodoUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.List;

@WebServlet("/deletetodo")
public class DeleteTodoServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameterMap().containsKey("id")) {
            int id = Integer.parseInt(request.getParameter("id"));
            TodoUser user = (TodoUser) request.getSession().getAttribute("user");
            System.out.println("Delete ID: " + id);
            user.removeTodo(id);

        } else {
            throw new ServletException("id is missing!");
        }

        // store data to XML
        try {
            JAXBHandler.marshal((List<TodoUser>) getServletContext().getAttribute("users"), new File("users.xml"));
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + "/todo");
    }

}
