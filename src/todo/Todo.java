package todo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "todo")
public class Todo {
    private int id;
    private String title;
    private String category;
    private String dueDate;
    private boolean important;
    private boolean completed;

    public Todo() {

    }

    public Todo(int id, String title, String category, String dueDate, boolean important) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.dueDate = dueDate;
        this.important = important;
        completed = false;
    }


    public boolean isOverDue() {
        if (dueDate != null) {
            if (getDueDateLocal().isBefore(LocalDate.now())) {
                return true;
            }
        }
        return false;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Todo)) return false;
        Todo todo = (Todo) o;
        return id == todo.id &&
                important == todo.important &&
                completed == todo.completed &&
                Objects.equals(title, todo.title) &&
                Objects.equals(category, todo.category) &&
                Objects.equals(dueDate, todo.dueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, category, dueDate, important, completed);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    //int year, int month, int dayOfMonth)
    public LocalDate getDueDateLocal() {
        String[] splittedDueDate = dueDate.split("\\.");
        return LocalDate.of(Integer.parseInt(splittedDueDate[2]), Integer.parseInt(splittedDueDate[1]), Integer.parseInt(splittedDueDate[0]));
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
