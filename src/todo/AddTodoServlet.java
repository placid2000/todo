package todo;

import Service.ValidatorService;
import User.TodoUser;
import User.JAXBHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/addtodo")
public class AddTodoServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("template", "add");
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("template", "add");

        TodoUser tempUser = (TodoUser) request.getSession().getAttribute("user");

        String title = request.getParameter("todoTitle");
        String category = (request.getParameter("todoCategory").equals("")) ? null : request.getParameter("todoCategory");
        String dueDate = (request.getParameter("todoDueDate").equals("")) ? null : request.getParameter("todoDueDate");
        //containsKey test should be enough since unchecked box is not passed to the request
        boolean important = request.getParameterMap().containsKey("important");

        ValidatorService validator = new ValidatorService();
        Map<String, String> errors = validator.validateRequestParam(title, dueDate);

        if(errors.size() > 0) {
            request.setAttribute("todoTitle", title);
            request.setAttribute("todoCategory", category);
            request.setAttribute("todoDueDate", dueDate);
            request.setAttribute("important", important);
            request.setAttribute("errors", errors);
            request.getRequestDispatcher("/index.jsp").forward(request, response);
            return;
        }

        Todo todo = new Todo(tempUser.getNextTodoID(), title, category, dueDate, important);
        tempUser.addTodo(todo);

        // store new data to XML
        try {
            JAXBHandler.marshal((List<TodoUser>) getServletContext().getAttribute("users"), new File("users.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + "/todo");
    }
}