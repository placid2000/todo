package todo;

import User.TodoUser;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");
        try (PrintWriter out = response.getWriter()) {
			
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("name");
        String password = request.getParameter("password");
		TodoUser todouser = new TodoUser(name, password);
		
        // save credentials in database


        // change to To Do application
        try {
            response.sendRedirect(request.getContextPath() + "/todo");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}