package todo;

import Service.ValidatorService;
import User.TodoUser;
import User.JAXBHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.rmi.ServerException;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/edittodo")
public class EditTodoServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        if(request.getParameterMap().containsKey("id")) {
            int id = Integer.parseInt(request.getParameter("id"));
            TodoUser user = (TodoUser) request.getSession().getAttribute("user");

            request.setAttribute("template", "edit");

            for(Todo todo : user.getTodos()) {
                if(todo.getId() == id) {
                    request.setAttribute("todo", todo);
                    request.getRequestDispatcher("/index.jsp").forward(request, response);
                    break;
                }
            }
        }

        throw new ServerException("Todo could not be found");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("template", "edit");

        TodoUser tempUser = (TodoUser) request.getSession().getAttribute("user");

        String title = request.getParameter("todoTitle");
        String category = (request.getParameter("todoCategory").equals("")) ? null : request.getParameter("todoCategory");
        String dueDate = (request.getParameter("todoDueDate").equals("")) ? null : request.getParameter("todoDueDate");
        //containsKey test should be enough since unchecked box is not passed to the request
        boolean important = request.getParameterMap().containsKey("important");


        //Whether we are in edit or add mode
        if(request.getParameterMap().containsKey("todoId")) {
            int id = Integer.parseInt(request.getParameter("todoId"));

            ValidatorService validator = new ValidatorService();
            Map<String, String> errors = validator.validateRequestParam(title, dueDate);

            if(errors.size() > 0) {
                Todo todo = new Todo(id, title, category, dueDate, important);
                request.setAttribute("todo", todo);
                request.setAttribute("errors", errors);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
                return;
            }

            for (Todo todo : tempUser.getTodos()) {
                if(todo.getId() == id) {
                    todo.setTitle(title);
                    todo.setCategory(category);
                    todo.setDueDate(dueDate);
                    todo.setImportant(important);
                    break;
                }
            }
        } else {
            throw new ServerException("Todo could not be found");
        }

        // store new data to XML
        try {
            JAXBHandler.marshal((List<TodoUser>) getServletContext().getAttribute("users"), new File("users.xml"));
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + "/todo");
    }
}