package Logger;

import javafx.application.Application;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public final class ApplicationLogger {
    private final static Logger logger;

    static {
        logger = Logger.getLogger(ApplicationLogger.class.getName());
    }

    private static FileHandler fh = null;

    public static void init(){
        try {
            fh=new FileHandler("loggerExample.log", true);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
        Logger l = Logger.getLogger("");
        fh.setFormatter(new SimpleFormatter());
        l.addHandler(fh);
        l.setLevel(Level.CONFIG);
    }

    /*
    public static void main(String[] args) {
        ApplicationLogger.init();

        logger.log(Level.INFO, "message 1");
        logger.log(Level.SEVERE, "message 2");
        logger.log(Level.FINE, "message 3");
        LoggerTest2.thing();
    }
    */
}