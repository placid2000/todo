<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="button_container">
    <a href="addtodo" class="btn">Todo erstellen</a>
</div>
<c:choose>
    <c:when test="${not empty param.filter}">
    <div class="reset-filter">
        <a href="/todo/todo"><i class="fas fa-filter"></i>Filter zurücksetzen</a>
    </div>
    </c:when>
 </c:choose>
<div>
    <table id="todoList">
        <thead>
        <tr>
            <th>Titel</th>
            <th class="resizer">Kategorie</th>
            <th class="resizer">Wichtig</th>
            <th>Frist</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${requestScope.todoList}" var="currentTodo">
                <tr>
                    <td>${currentTodo.title}</td>
                    <td class="resizer">${currentTodo.category}</td>
                    <td  class="resizer">
                        <c:choose>
                            <c:when test="${currentTodo.important}">
                                Ja
                            </c:when>
                            <c:otherwise>
                                Nein
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <span <c:if test="${currentTodo.isOverDue()}">class="overdue"</c:if>>
                            ${currentTodo.dueDate}
                        </span>
                    </td>
                    <td class="table-tool">
                        <a href="/todo/edittodo?id=${currentTodo.id}"><i class="fas fa-edit"></i></a>
                        <a href="/todo/deletetodo?id=${currentTodo.id}"onclick="return confirm('Wollen Sie diese todo wirklich löschen?')"><i class="fas fa-trash-alt"></i></a>
                        <a href="/todo/complete?id=${currentTodo.id}"><i class="fas fa-clipboard-check"></i></a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
