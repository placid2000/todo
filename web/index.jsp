<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>Todo Applikation</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="js/pickadate/themes/classic.css">
        <link rel="stylesheet" href="js/pickadate/themes/classic.date.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <header>
            <div class="container">
                <div id="branding">
                    <h1>Todo Applikation</h1>
                </div>
                <nav>
                    <ul>
                        <li><a href="logout">Ausloggen</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="container clearfix">
            <section class="categories">
                <aside>
                    <div class="subtitle">
                        <h3>Kategorien</h3>
                    </div>
                    <nav>
                        <ul id="categoryList">
                            <c:set var="filterParam" scope = "request" value = "${param.filter}"/>
                            <li><a href="/todo/todo?filter=offen" <c:if test="${param.filter.equals(\"offen\")}">class="active"</c:if>>Offen</a></li>
                            <li><a href="/todo/todo?filter=erledigt" <c:if test="${param.filter.equals(\"erledigt\")}">class="active"</c:if>>Erledigt</a></li>

                            <c:forEach items="${categories}" var="category">
                                <li><a href="/todo/todo?filter=${category}" <c:if test="${param.filter.equals(category)}">class="active"</c:if>>${category}</a></li>
                            </c:forEach>

                        </ul>
                    </nav>
                </aside>
            </section>
            <section class="title">
                <c:if test="${requestScope.template.equals(\"list\")}">
                    <p>Meine Todo's</p>
                </c:if>
                <c:if test="${requestScope.template.equals(\"add\")}">
                    <p>Neue Todo erstellen</p>
                </c:if>
                <c:if test="${requestScope.template.equals(\"edit\")}">
                    <p>Todo bearbeiten</p>
                </c:if>
            </section>
            <section class="content">
                <c:import url="${requestScope.template}.jsp" />
                <footer>
                    <p>TODO Application, Copyright &copy; 2019</p>
                </footer>
            </section>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="js/pickadate/picker.js"></script>
        <script src="js/pickadate/picker.date.js"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                var datePicker = $('.datepicker').pickadate({
                    format: 'dd.mm.yyyy'
                });
            });
        </script>
    </body>
</html>

