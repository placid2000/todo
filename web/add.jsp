<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="form-box">
    <form method="post" action="addtodo">
        <div class="form-field">
            <input type="text" name="todoTitle" placeholder="Titel" value="${requestScope["todoTitle"]}" />
        </div>
        <c:if test="${requestScope.errors.containsKey('title')}">
            <div class="alert">${requestScope.errors["title"]}</div>
        </c:if>
        <div class="form-field">
            <input type="text" name="todoCategory" placeholder="Kategorie" value="${requestScope["todoCategory"]}" />
        </div>
        <div class="form-field">
            <input class="datepicker" type="text" placeholder="Fristdatum" name="todoDueDate" value="${requestScope["todoDueDate"]}" />
        </div>
        <c:if test="${requestScope.errors.containsKey('dueDate')}">
            <div class="alert">${requestScope.errors["dueDate"]}</div>
        </c:if>
        <div class="form-field">
            <input type="checkbox" id="important_checkbox" name="important" <c:if test="${requestScope['important']}">checked</c:if>/>
            <label for="important_checkbox" id="label-checkbox-important">Wichtig</label>
        </div>
        <div class="form-field form-buttons clearfix">
            <a class="btn" href="/todo/todo">Zurück</a>
            <input class="btn" type="submit" value="Speichern" />
        </div>
    </form>
</div>