<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <title>Todo Applikation</title>
    </head>
    <body>
    <div id="box-login">
        <form class="form-login" method="post" action="/todo/login">
            <fieldset>
                <legend>ToDo Login</legend>
                <input type="text" name="name" placeholder="Username" />
                <input type="password" name="password" placeholder="Password" />
                <input type="submit" value="Log In" />
                <input class="facebook" type="submit" value="Login with Facebook!" />
            </fieldset>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $('.facebook').on('click', function(e){
            alert('HA!');
        });
    </script>
    </body>
</html>