<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="form-box">
    <form method="post" action="edittodo">
        <input type="hidden" name="todoId" value="${todo.getId()}" />
        <div class="form-field">
            <input type="text" name="todoTitle" placeholder="Titel"  value="${todo.getTitle()}" />
        </div>
        <c:if test="${requestScope.errors.containsKey('title')}">
            <div class="alert">${requestScope.errors["title"]}</div>
        </c:if>
        <div class="form-field">
            <input type="text" name="todoCategory" placeholder="Kategorie" value="${todo.getCategory()}" />
        </div>
        <div class="form-field">
            <input type="text" class="datepicker" placeholder="Fristdatum" name="todoDueDate" value="${todo.getDueDate()}" pattern="dd.MM.yyyy"/>
        </div>
        <c:if test="${requestScope.errors.containsKey('dueDate')}">
            <div class="alert">${requestScope.errors["dueDate"]}</div>
        </c:if>
        <div class="form-field">
            <c:choose>
                <c:when test="${todo.isImportant()}">
                    <input type="checkbox" id="important" name="important" checked />
                </c:when>
                <c:otherwise>
                    <input type="checkbox" id="important" name="important" />
                </c:otherwise>
            </c:choose>
            <label for="important" id="label-checkbox-important">Wichtig</label>
        </div>
        <div class="form-field form-buttons clearfix">
            <a class="btn" href="/todo/todo">Zurück</a>
            <input class="btn" type="submit" value="Speichern" />
        </div>
    </form>
</div>

